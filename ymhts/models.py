from django.db import models

class YMHT(models.Model):
    first_name = models.CharField(max_length=255)
    mobile = models.CharField(max_length=10)

    def __unicode__(self):
        return "%s --> %s" % (self.first_name, self.mobile)
